History behavior for Yii2
=========================

This extension allows you to store any attribute change happening to an ActiveRecord


Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```sh
php composer.phar require yiidk/yii2-history-behavior "*"
```

or add

```json
"yiidk/yii2-history-behavior": "*"
```

to the require section of your `composer.json` file.


Configuration
-------------

You need to configure your model as follows:

```php
class Category extends ActiveRecord
{
	public function behaviors() {
		return [
			[
				'class' => HistoryBehavior::className(),
				'tableName' => 'history', // `history is the default table name
                'blacklistedFields' => [], // these fields won't be taken in account in the diff
			],
		];
	}

}
```

You also need to create the history table. A sample is provided in the `schema` folder
