<?php

namespace yiidk\behaviors;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property string $action
 * @property string $tb
 * @property string $pk
 * @property string $data
 * @property string $created_at
 * @property integer $user_id
 * @property string $ip
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action', 'tb', 'pk', 'created_at', 'ip'], 'required'],
            [['action', 'data'], 'string'],
            [['created_at'], 'safe'],
            [['user_id'], 'integer'],
            [['tb', 'pk', 'ip'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'action' => Yii::t('app', 'Action'),
            'tb' => Yii::t('app', 'Tb'),
            'pk' => Yii::t('app', 'Pk'),
            'data' => Yii::t('app', 'Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'user_id' => Yii::t('app', 'User ID'),
            'ip' => Yii::t('app', 'Ip'),
        ];
    }
}
