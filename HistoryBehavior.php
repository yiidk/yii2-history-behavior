<?php
/**
 * @link https://bitbucket.org/yiidk/yii2-history-behavior
 */

namespace yiidk\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * @author Mehdi Achour <machour@gmail.com>
 */
class HistoryBehavior extends Behavior
{

    /**
     * The create operation
     */
    const OPERATION_CREATE = 'create';
    /**
     * The update operation
     */
    const OPERATION_UPDATE = 'update';
    /**
     * The delete operation
     */
    const OPERATION_DELETE = 'delete';

    /**
     * The history table name
     */
    public $tableName = 'history';

    /**
     * @var ActiveRecord the owner of this behavior.
     */
    public $owner;

    /**
     * @var Array blacklisted fields
     */
    public $blacklistedFields = [];

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT  => 'afterInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attach($owner)
    {
        $this->owner = $owner;
        parent::attach($owner);
    }

    /**
     * @inheritdoc
     */
    public function afterInsert($event) {
        $this->saveChanges(self::OPERATION_CREATE);
    }

    /**
     * @inheritdoc
     */
    public function beforeUpdate($event) {
        $this->saveChanges(self::OPERATION_UPDATE);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete($event) {
        $this->saveChanges(self::OPERATION_DELETE);
    }

    /**
     * Saves modifications to the history table
     * 
     * @param string $action The history action being recorded
     */
    private function saveChanges($action)
    {
        $id = null;

        $diff = $this->diff($action); // == 'delete' ? $this->owner->attributes : $this->owner->dirtyAttributes;

        if (!empty($diff)) {
            if (count($this->owner->tableSchema->primaryKey) == 1) {
                $id = $this->owner->{$this->owner->tableSchema->primaryKey[0]};
            }

            \yii::$app->db->createCommand(
                sprintf("INSERT INTO `%s` (`tb`, `pk`, `action`, `data`, `user_id`, `ip`, `created_at`) 
                    VALUES('%s', '%s', '%s', '%s', %s, '%s', NOW())",
                    $this->tableName,
                    $this->owner->tableSchema->name, 
                    $id,
                    $action,
                    addslashes(json_encode($diff, true)),
                    \yii::$app->user->isGuest ? 'NULL' : \yii::$app->user->id,
                    \yii::$app->request->userIP)
            )->execute();
        }
    }

    /**
     * Gets the history diff to be saved in the table
     * 
     * @param string $action The history action being recorded
     * @return  array description
     */
    private function diff($action)
    {

        $result = [];

        switch ($action) {
            case self::OPERATION_DELETE:
            foreach ($this->owner->attributes as $key => $value) {
                if (in_array($key, $this->blacklistedFields)) {
                    continue;
                }
                $result[$key] = [$value, null];
            }
            break;

            case self::OPERATION_CREATE:
            foreach ($this->owner->attributes as $key => $value) {
                if (in_array($key, $this->blacklistedFields)) {
                    continue;
                }
                $result[$key] = [null, $value];
            }
            break;

            case self::OPERATION_UPDATE:
            $oldAttributes = $this->owner->oldAttributes;
            foreach ($this->owner->dirtyAttributes as $key => $value) {
                if (in_array($key, $this->blacklistedFields)) {
                    continue;
                }
                if (isset($oldAttributes[$key]) && $oldAttributes[$key] != $value) {
                    $result[$key] = [$oldAttributes[$key], $value];
                } else {
                    $result[$key] = [null, $value];                    
                }
            }
            break;
        }

        unset($result[$this->owner->tableSchema->primaryKey[0]]);

        return $result;
    }

}
