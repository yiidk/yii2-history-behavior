CREATE TABLE `history` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `action` enum('create','update','delete') NOT NULL,
 `tb` varchar(15) NOT NULL,
 `pk` varchar(15) NOT NULL,
 `data` text,
 `created_at` datetime NOT NULL,
 `user_id` smallint(5) unsigned DEFAULT NULL,
 `ip` varchar(15) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_history_user_idx` (`user_id`),
 KEY `tb_pk` (`tb`,`pk`),
 CONSTRAINT `fk_history_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8
